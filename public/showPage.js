/* global chrome*/
chrome.browserAction.onClicked.addListener(() => showPage('index.html#manager', true));

function showPage(page, openInNewWindow) {
  const url = chrome.extension.getURL(page); 
  chrome.tabs.query(
    {currentWindow: !openInNewWindow, url},
    tabs => {
      if (!tabs.length)
        openInNewWindow
        ? chrome.windows.create({url, focused: true, type: 'popup'})
        : chrome.tabs.create({url, active: true})
      else
        chrome.tabs.update(tabs[0].id, {active: true},
          ({windowId}) =>
            chrome.windows.update(windowId, {focused: true})
        )
    }
  );
}