import React from 'react'
import ReactDOM from 'react-dom'

import Manager from './Manager'
import Bookmarks from './components/Bookmarks'

import {urlParser} from './utils'

import browser from './browser'

const url = urlParser(window.location.hash.replace(/#/g, '/')),
  [entity, identifier] = url.pathname,
  render = el => ReactDOM.render(
    el,
    document.getElementById('workspaces')
  )

switch (entity) {
  case 'manager':
    browser.windows.get('*', {populate: true})
    .then(windows =>
      //chrome.tabs.getCurrent(current =>
        render(<Manager {...{windows/*, current*/}}/>)
      //)
    )
    break
  case 'bookmarks':
    if (identifier)
      browser.bookmarks.get(identifier)
      .then(({children}) =>
        browser.windows.create({
          url:
            children.map(({url}) => url).filter(v => v)
        })
      )
      .then(() => browser.tabs.currentDelete())
    else
      browser.bookmarks.getTree()
      .then(({children}) =>
        render(<Bookmarks bookmarks={children}/>)
      )
    break
  default:
}
