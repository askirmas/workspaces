/* global chrome */
import React from 'react'

export function id() {
  return null
}

export function windowId({windowId, id}, windowsIds) {
  return <select {...commonProps({name: 'windowId', id})}
    defaultValue={windowId}
    onChange={({target: {value}}) => { 
      if (value === 'new')
        chrome.windows.create({tabId: id})
      else
        chrome.tabs.move(id, {windowId})
    }}
  >
    Object.entries(windowsIds)
    .filter(([_, value]) => value)
    .concat([['new']])
    .map(([id]) => <option value={id}>{id}</option>)
  }</select>
}

export function index({index, id}) { 
  return <div {...commonProps({name: 'index', id})}>#{index}</div>
} 

export function favIconUrl({favIconUrl, id}) {
  return <img {...commonProps({name: 'favIconUrl', id})}
    alt=""
    src={favIconUrl}
  />
}

export function url({url, index, windowId, id}) {
  let hostname;
  try {
    hostname = new URL(url).hostname
  } catch (e) {}

  return <button {...commonProps({name: 'url', id})}
    onClick={() => chrome.tabs.highlight({tabs: index, windowId})}
  >{
    hostname
  }</button>
}

export function title({id, title}) {
  return <div {...commonProps({name: 'title', id})}>{title}</div>
}

export function close({id}) {
  return <button {...commonProps({name: 'close', id})}
    onClick={() => chrome.tabs.remove(id)}
  >
    Close
  </button>
}

export function path({path}) {
  return <div
    className="path"
  >{
    path.map(({title}) => title).join(' / ')
  }</div>
}

function commonProps({id, name}) {
  return {
    className: name,
    key: [id, name].join('/')
  }
}