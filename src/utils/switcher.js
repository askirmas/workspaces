function caser(...args/*...factors ,action*/) {
  if (this.resolved || args.length <= 1)
    return this  
  const action = args[args.length - 1],
    factors = args.slice(0, -1)

  return typeof factors[0] !== 'function' 
  && this.value !== factors[0]
  || typeof factors[0] === 'function'
  && !factors[0](this.value)
  ? caser.apply(this, args.slice(1))
  : Object.assign(this, {
    resolved: true,
    value: callIfPossible(action)
  })
}

function defaulter(value) {
  return this.resolved
  ? this.value
  : callIfPossible(value)
}

function switcher(value) {
  return {
    value,
    resolved: false,
    case: caser,
    default: defaulter
  }
}

function callIfPossible(source) {
  if (typeof source !== 'function')
    return source
  return source()
}

module.exports = switcher