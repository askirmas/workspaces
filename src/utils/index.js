function tree2list(node, path = []) {
  if (Array.isArray(node)) {
    return Object.assign({},
      ...node.map(el => tree2list(el, path))
    )
  }

  const {id, children} = node,
    newNode = Object.assign({}, node, {path})
  if (!children)
    return {[id]: newNode}

  return Object.assign(
    {
      [id]: Object.assign(newNode, {
        children: children.map(({id}) => id)
      })
    },
    tree2list(children, [...path, id])
  )
} 
  
function urlFormEncodedParser(str) {
  return Object.assign({},
    ...[
      ...new URLSearchParams(
        str.replace(/^[?#]/, '')
      )
    ]
    .map(([k, v]) => ({[k]: v})) 
  )
}

function urlParser(str) {
  const url = new URL(
      str.replace(/^[?#]/, ''),
      'http://x'
    )

  return {
    pathname: url.pathname.split('/').filter(v => v !== ''),
    searchParams: url.searchParams,
    query: Object.assign({},
      ...[
        ...url.searchParams.entries()
      ].map(([key, value]) => ({[key]: value}))
    )
  }
}

function nop() {}

function idfn(...args) {
  return args.length === 0
  ? undefined
  : args.length === 1 
  ? args[0]
  : args 
} 

function getType(source) {
  return source === null
  ? 'null'
  : Array.isArray(source)
  ? 'array'
  : parseInt(source) == source
  ? 'int'
  : parseFloat(source) == source
  ? 'number'
  : typeof source
}

function tree2table(nodes, buffer = {}) {
  if (nodes.length === 0)
    return buffer;
  const node = nodes.shift(),
    {children = [], parentId = null} = node
  if (children.length !== 0)
    Object.assign(node, {
      children: children.map(({id}) => id) 
    })
  if (parentId !== null) {
    const {path: parentPath = []} = (buffer[parentId] || {})
    Object.assign(node, {
      path: [
        ...parentPath,
        parentId
      ]
    })
  }
   return tree2table(
    [...children, ...nodes],
    Object.assign(buffer, {
        [node.id]: node
    })
  )
} 

export {
  nop,
//  idfn,
  tree2list,
  getType,
  urlFormEncodedParser,
  urlParser
}