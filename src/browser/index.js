export default require(
  window.location.hostname === 'chrome' 
  ? './chrome'
  : './localhost'
).default