
import switcher from '../../utils/switcher'
import {getType, tree2list} from '../../utils'
import bookmarkNodes from './bookmarksTree.json'

export default {
  windows: {
    _holder: {},
    async create({url}) {
      (Array.isArray(url) ? url : [url])
      .forEach(href => {
        const a = document.createElement('a')
        a.setAttribute('href', href)
        a.setAttribute('target', '_blank')
        a.setAttribute('style', 'display:none;')
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
      })
      return {id : 0}
    }
  },
  bookmarks: {
    _holder: {},
    async getTree() {
      return bookmarkNodes
    },
    async get(query) {
      const list = tree2list(bookmarkNodes)
      return switcher(getType(query))
      .case('int', () => {
        if (query in list)
          return list[query]
        else {
          throw new Error(`No ${query} in list`)
        }
        
        /*Object.assign(list[query], {
          children: list[query].children.map(id => list[id])
        })*/
      })
      .case('object', () => {
        const {url} = query
        let filtered = list
        if (url)
        filtered = Object.entries(filtered)
          .filter(([id, node]) => url === node.url)
          .map(([_, node]) => node)
        return filtered
      })
      .default(() => {
        throw new Error('Unknown .get query')
      })
    }
  },
  tabs: {}
}
