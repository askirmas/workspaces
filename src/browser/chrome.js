/* global chrome */
import {getType} from '../utils'
import switcher from '../utils/switcher'

const $default = [(_, reject) => reject()]

export default {
  windows: {
    _holder: chrome.windows,
    create(options = {}) {
      return new Promise(resolve =>
        this._holder.create(options, resolve)
      )
    },
    get(id = '?', options = {}) {
      const idInt = parseInt(id)
      return new Promise(...(
        idInt
        ? [resolve => this._holder.get(idInt, options, resolve)]
        : switcher(id)
        .case('*', [resolve => this._holder.getAll(options, resolve)])
        .case('?', [resolve => this._holder.getCurrent(options, resolve)])
        .case('last', [resolve => this._holder.getLastFocused(options, resolve)])
        .default(() => $default)
      ))
    }
  },
  tabs: { 
    _holder: chrome.tabs,
    create(options = {}) {
      return new Promise(resolve =>
        this._holder.create(options, resolve)
      )
    },
    delete(tabs) {
      return new Promise(
        getType(tabs) === 'object'
        ? resolve =>
          this._holder.remove(tabs.id, resolve)
        : resolve =>
          this._holder.remove(tabs, resolve)
      )
    },
    currentGet() {
      return new Promise(resolve =>
        this._holder.getCurrent(resolve)
      )
    },
    currentDelete() {
      return this.currentGet()
      .then(tab =>
        this.delete(tab)
      )
    },
    on: {
      _holder: chrome.bookmarks,
      created: listener => {
        const fn = tab => listener(tab.id, undefined, tab)
        this._holder.onCreated.addListener(fn)
        return () => this._holder.onCreated.removeListener(fn)
      },
      updated: listener => {
        this._holder.onUpdated.addListener(listener)
        return () => this._holder.onUpdated.removeListener(listener)
      },
      attached: listener => {
        const fn = (id, {newWindowId, newPosition}) => listener(id, undefined, {
          index: newPosition,
          windowId: newWindowId
        }) 
        this._holder.onAttached.addListener(fn)
        return () => this._holder.onAttached.removeListener(fn)
      },
      removed: listener => {
        const fn = (id, info) => listener(id, info, null) 
        this._holder.onRemoved.addListener(fn)
        return () => this._holder.onRemoved.removeListener(fn)
      }
    }  
  },
  bookmarks: {
    _holder: chrome.bookmarks, 
    isFolder({url = null}) {
      return url === null
    },
    isLeaf({url = null}) {
      return url !== null
    },
    leaves(bookmarks) {
      return bookmarks.filter(this.isLeaf)
    },
    getTree(query = null) {
      return new Promise(
        ...switcher(getType(query))
        .case('null', [resolve => 
          this._holder.getTree(nodes => resolve(nodes[0]))
        ])
        .case('int', 'string', [resolve =>
          this._holder.getSubTree(query.toString(), nodes => resolve(nodes[0]))
        ])
        .case('object', [resolve =>
          this._holder.getSubTree(query, nodes => resolve(nodes[0]))
        ])
        .default(() => $default)
      )
    },
    get(query) {
      const appendChildren = (id, roots, resolve) =>
        this._holder.getChildren(id,
          children => resolve(Object.assign(roots[0], {children}))
        )
      return new Promise(
        ...switcher(getType(query))
        .case('int', [resolve => {
          const id = query.toString()
          this._holder.get(id,
            roots => appendChildren(id, roots, resolve)
          )
        }])
        .case('object', [resolve =>
          this._holder.search(query, resolve)
        ])
        // TODO: path?
        .case('string', () => $default)
        .default(() => $default)
      )
    }
  }
}