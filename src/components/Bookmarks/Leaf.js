import React from 'react'

export default function({title, id, url, handlers}) {
  return <div key={id + ".title"}
    {...{
      className: "Leaf Title",
      "data-id": id,
      "data-url": url
    }}
    {...handlers}
  >{
    title
  }</div>
}
