import React from 'react'

import Folder from './Folder'
import Leaf from './Leaf'
import Details from './Details'

import browser from '../../browser'

export default class Bookmarks extends React.Component {
  state = {
    folderId: null,
    details: null,
    path: []
  }
  title = "Bookmarks"

  constructor(props) {
    super(props)
    const {
      id = null,
      path = []
    } = (props.bookmarks[0] || {})
    this.state = {folderId: id, path}
  }
  
  componentDidMount() {
    document.title = 'Bookmarks - Workspaces'
  }

  render() {
    const {bookmarks} = this.props,

      folders = bookmarks.map(data =>
        <Folder key={data.id} {...{
          className: 'Folder',
          forDepth: 
            depth => ({
              style: {
                paddingLeft: `calc(${depth} * var(--indentation))`
              }
            }),
          handlers: {
            onClick: ({target}) => {
              const folderId = target.getAttribute('data-id')
              if (folderId === null)
                return;
              this.setState({
                folderId,
                path: (target.getAttribute('data-path') || '')
                .split(',').filter(v => v !== '')
              })
            }
          },
          data
        }}>
          <button
            className="Action Open"
            key={data.id + '/open'}
            onClick={async ({target}) => {
              const {children} = await browser.bookmarks.get(
                target.parentElement.getAttribute('data-id')
              )
              browser.windows.create({
                url:
                  children.map(({url}) => url).filter(v => v)
              })
            }}
          >open</button>
        </Folder>
      ),

      leaves = this.state.folderId
      && evaluatePath(bookmarks, [...this.state.path, this.state.folderId])
      .filter(({url = null}) => url !== null) //not filter url === '' 
      .map(data => <Leaf key={data.id}
        {...data}
        handlers={{
          onClick: async ({target}) => {
            const url = target.getAttribute('data-url')
            if (!url)
              return;
            const details = await getPaths({url})
            this.setState({details})
          }
        }}    
      />)
      
    return <div
      className="Bookmarks Container"
    >
      <div>{folders}</div>
      <div>{leaves}</div>
      {
        this.state.details
        && <div><Details data={this.state.details}/></div>
      }
    </div>
  }
}

function evaluatePath(candidates, path) {
  if (!Array.isArray(path) ||!Array.isArray(candidates) || path.length === 0 || candidates.length === 0)
    return candidates
  const nodeId = path[0],
    {children = []} = (candidates.find(({id}) => id === nodeId) || {})
  return evaluatePath(
    children,
    path.slice(1)
  )
}


function getPath(promise, path = []) {
  return promise.then(({id, parentId, title}) => {
    const nextPath = [{id, title}, ...path]
    return parentId === undefined
    ? nextPath
    : getPath(browser.bookmarks.get(parentId), nextPath)
  })
}

function getPaths(query) {
  return browser.bookmarks.get(query)
  .then(bms => Promise.all(
    (Array.isArray(bms) ? bms : [bms])
    .map(({id}) => getPath(browser.bookmarks.get(id)))
  ))
}