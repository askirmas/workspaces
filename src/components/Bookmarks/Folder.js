import React from 'react'

export default class Folder extends React.Component {
  state = {
    expand: false
  }
  render() {
    const {
      className = '',
      data: {
        id,
        children,
        title  
      },
      children: reactChildren = null,
      depth = 0,
      forDepth = _ => {},
      path = [],
      handlers = {}
    } = this.props
    
    if (!children)
      return null;

    const expandButton = <button {...{
        className: 'ExpandButton',
        key: id + '/ExpandButton',
        onClick: () => this.setState({expand: !this.state.expand}),
        style: children.some(child => 'children' in child)
        ? {}
        : {visibility: 'hidden'}
      }}
      >{
        this.state.expand ? '-' : '+'
      }</button>,

      childrenFolders = !this.state.expand
      ? null
      : children.map(data =>
        <Folder
          {...{
            key: data.id,
            depth: depth + 1,
            forDepth,
            className,
            handlers,
            path: [...path, id],
            data
          }}
        >{
          reactChildren
        }</Folder>
      ),

      face = <div {...{
          key: id + '/Title',
          className: 'Title',
          'data-id': id,
          'data-path': path.join(',')
        }}
        {...handlers}
      >{[
        title,
        reactChildren
      ]}</div>

    return !className
    ? [expandButton, face, childrenFolders]
    : [
      <div {...{
          key: id,
          className
        }}
        {...forDepth(depth)}
      >{[
        expandButton,
        face
      ]}</div>,
      childrenFolders
    ]
  }
}
