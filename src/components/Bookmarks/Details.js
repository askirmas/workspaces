import React from 'react'

export default function({data}) {
  return data.map(trajectory => <div>{ 
    trajectory.map(({title, id}, i) => [
      i > 1 ? ' / ' : null,
      <a href={id}>{title}</a>
    ])
  }</div>)
}