/* global chrome */
import React from 'react'
import {nop} from './utils'
import {
  id,
  windowId,
  index,
  favIconUrl,
  url,
  title,
  close
} from './Viewer' 
const Viewer = {
  id,
  windowId,
  index,
  favIconUrl,
  url,
  title,
  close
}

export default class App extends React.Component {
  state = {
    windowIds: {},
    tabIds: []
  }
  
  classes = ['Tab', 'Container']

  tab2stateObject(tab) {
    return tab === null
    ? null
    : Object.entries(tab)
    .reduce(
      (acc, [key, value]) =>
        key in Viewer
        ? Object.assign(acc, {[key]: value})
        : acc,
      {}
    )
  }

  handlersPlug(remove) {
    const handlers = {tabs: [
      '',
      'onCreated',
      'onUpdated',
      'onRemoved',
      'onAttached'
    ]}

    Object.entries(handlers)
    .forEach(([entity, events]) =>
      events.forEach(event => {
        const fnName = this.fnName(entity, event)
        if (!remove)
          this[fnName] = this[fnName].bind(this)
        if (entity && event)
          chrome[entity][event][
            `${
              remove ? 'remove' : 'add'
            }Listener`
          ](
            this[fnName]
          )
      })
    );
  }
  
  fnName(entity, event) {
    return ['handler', entity, event].filter(v => v !== '').join('_')
  }

  handler_tabs(id, _, tab) {
    const key = `t${id}`,
      data = this.tab2stateObject(tab),
      isNew = !(data === null || key in this.state)
    if (data === null)
      return this.setState({[key]: null}, () => 
        this.setState({tabIds: this.state.tabIds.filter(v => v !== id)})
      )
    const {windowId = null} = data
    if (windowId && !(windowId in this.state.windowIds))
      this.setState({windowIds: Object.assign({}, this.state.windowIds, {[windowId]: true})})
    this.setState(
      {
        [key]: Object.assign({},
          !isNew
          ? {}
          : this.state[key],
          data
        )
      },
      !isNew 
      ? nop
      : () => this.setState({
        tabIds: [...this.state.tabIds, id]
      })
    )
  }

  handler_tabs_onCreated(tab) {
    return this.handler_tabs(tab.id, undefined, tab)
  }
  handler_tabs_onUpdated(id, _, tab) {
    return this.handler_tabs(id, undefined, tab)
  }  
  handler_tabs_onAttached(id, {newWindowId, newPosition}) {
    return this.handler_tabs(id, undefined, {index: newPosition, windowId: newWindowId});
  }
  handler_tabs_onRemoved(id, info) {
    return this.handler_tabs(id, undefined, null)
  }
/*  handler_tabs_onMoved(tab) {
//    Only one move event is fired, representing the tab the user directly moved. Move events are not fired for the other tabs that must move in response to the manually-moved tab.
so indexes in window should be recalcualted
  }
*/

  constructor(props) {
    super(props)
    this.state.windowIds = props.windows.reduce(
      (acc, {id, focused, incognito, type, state, tabs}) => {
        this.state[`w${id}`] = {focused, incognito, type, state}
        this.state.tabIds = [...this.state.tabIds,
          ...tabs
          .map(tab => {
            if (tab.id === props.current)
              return null
            this.state[`t${tab.id}`] = this.tab2stateObject(tab)
            return tab.id
          }).filter(v => v !== null)
        ];
        acc[id] = true
        return acc
      },
      {}
    )
    this.handlersPlug(false)
  }

  componentWillUnmount() {
    this.handlersPlug(true)
  }

  render() {
    const cssClass = '.' + this.classes.join('.')
    return <>
      <style>{`
        ${cssClass} {
          grid-template-columns: repeat(${Object.keys(Viewer).length}, max-content);
        }
        ${
          Object.keys(Viewer)
          .map((field, i) => `${cssClass} .${field} {
            grid-column: ${i + 1};
          }`)
          .join('')
        }
      `}</style>
      <div {...{
        className: this.classes.join(' ')
      }}>{
        this.state.tabIds.map(id => {
          const tab = this.state[`t${id}`]
          return id === null
          ? null
          : tab === null || typeof tab !== 'object' 
          ? null
          : Object.values(Viewer)
          .map(fn => fn(tab, this.state.windowIds))
        })
      }</div>
    </>
  }
}
