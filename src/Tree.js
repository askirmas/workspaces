import React from 'react'

export default class Tree extends React.Component {
  state ={
    expand: false 
  }

  constructor(props){
    super(props)
    this.state.expand = props.expand || false
  }

  render() {
    const {title, children = [], id, depth = 0, url, extract = false} = this.props,
      isFolder = url === undefined,
      classified = children.reduce(
        (acc, child) => {
          acc[
            child.url === undefined
            ? 'folders'
            : 'leaves'
          ].push(child)
          return acc
        },
        {
          folders: [],
          leaves: []
        }
      )
    return [
      extract
      ? null
      : <div
        key={id}
      >{[
        "|",
        "-".repeat(depth),

        !url
        ? [
          <button
            onClick={() => this.setState({expand: !this.state.expand})}
          >{
            this.state.expand ? '-' : '+'
          }</button>,
          title
        ]
        : <a href={url}>{title}</a>
      ]}</div>,

      !isFolder || extract
      ? null
      : <div>{
        !this.state.expand
        ? null
        : classified.leaves.map(({title, url}) => <div><a href={url}>{title}</a></div>)
      }</div>,

      !this.state.expand
      ? null
      : classified.folders.map(child => <Tree depth={depth + 1} {...child}/>)
    ]
  }
}